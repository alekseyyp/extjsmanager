<?php
/**
 * @author Aleksey Prozhoga
 */
return array(
   'service_manager' => array(
   		'factories'	=> array(
   			//"ExtjsManager" => "ExtjsManager\Service\Assets\DebugFactory"
   		)
   ),
	'controller_plugins' => array(
		'invokables' => array(
			'ExtjsManager'	=> 'ExtjsManager\Controller\Plugin\ExtjsManager',
		)
	)
);
