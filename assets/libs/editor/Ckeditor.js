Ext.namespace('CustomLib.editor');

Ext.define('CustomLib.editor.Ckeditor', {
	extend : 'Ext.form.field.TextArea',
	alias : 'widget.ckeditor',
	xtype: 'ckeditor',
	initComponent : function(){
		this.callParent(arguments);
		this.on('afterrender', function(){
			/*Ext.apply(this.CKConfig ,{
				height : this.getHeight()
			});*/
			this.editor = CKEDITOR.replace(this.inputEl.id,this.CKConfig);
			this.editorId =this.editor.id;
		},this);
	},
	onRender : function(ct, position){
		if(!this.el){
			this.defaultAutoCreate ={
				tag : 'textarea',
				autocomplete : 'off'
			};
		}
		this.callParent(arguments)
	},
	setValue  : function(value){
		this.callParent(arguments);
		if(this.editor){
			this.editor.setData(value);
		}
	},
	getRawValue: function(){
		if(this.editor){
			return this.editor.getData()
		}else{
			return''
		}
	}
});