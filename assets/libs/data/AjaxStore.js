/* 
 * @author Aleksey Prozhoga
 */
Ext.namespace('CustomLib.data');

Ext.define('CustomLib.data.AjaxStore', {
	extend: 'Ext.data.Store',
	pageSize: 10,
	remoteSort: true,
	root: 'data',
	listeners: {
		datachanged: function( Store, eOpts ){
			var a = Store;
		}
	},
	autoLoad: true,
	constructor: function(config){
		
		var me = this;
		if(config.property && config.dir){
			me.sorters = [{
				property: config.property,
				direction: config.dir
			}];
		}
		Ext.apply(me, config);
		
		this.rebuildProxy();
		
		this.callParent(config);
	},
	rebuildProxy: function(extraParams){
		var proxy  = new Ext.data.proxy.Ajax({
			url: this.url,
			simpleSortMode: true,
			extraParams: extraParams ? extraParams : null,
			reader: {
				type: 'json',
				root: this.root
			}
		});
		this.proxy = proxy;
		this.setProxy(proxy);
	}
});
