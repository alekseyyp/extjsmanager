/* 
 * @author Aleksey Prozhoga
 */
Ext.namespace('CustomLib.window');

Ext.define("CustomLib.window.Message", {
	title: "Message",
	content: "",
	id: null,
	el: null,
	timeout: 3000,
	constructor: function(config){
		this.id = Ext.id();
		_this = this;
		_this = Ext.apply(_this, config, {
			title: this.title
		});
		
		var dh = Ext.DomHelper; // create shorthand alias
		
		// specification object
		var spec = {
		    id: this.id,
		    tag: 'div',
		    cls: 'msg-div',
		    children: [
		        {tag: 'div', cls: 'msg', children: [
		        	{tag: 'h3', html: this.title},
		        	{tag: 'p', html: this.content}
		        ]},
		    ]
		};
		this.el = dh.append(
		    Ext.getBody(),
		    spec, true
		);
		Ext.defer(function(){
			this.el.fadeOut({
				remove: true,
				easing: 'easeOut',
			    duration: 500
			});
		}, this.timeout, this);
	}
});