/* 
 * @author Aleksey Prozhoga
 */
Ext.namespace('CustomLib.window');

Ext.define('CustomLib.window.WindowForm', {
	extend: 'Ext.window.Window',
	title: '',
	closable: true,
	closeAction: 'hide',
	width: 600,
	minWidth: 350,
	height: 350,
	layout: 'fit',
	modal: true,
	bodyStyle: 'padding: 5px;',
	//Custom properties
	titles: {
		add: 'Add New',
		edit: 'Editing'
	},
	urls: {
		add: "/add",
		edit: "/edit"
	},
	btnCancel: 'Cancel',
	btnSubmit: 'Submit',
	mode: 'add',
	form: null,
	formConfig: {},
	initComponent: function(){
		var formConfig = {
			plain: true,
			border: 0,
			bodyPadding: 5,
			url: null,

			fieldDefaults: {
				labelWidth: 155,
				anchor: '100%'
			},

			layout: {
				type: 'vbox',
				align: 'stretch'  // Child items are stretched to full width
			},
			buttons: [{
				text: this.btnCancel,
				handler: Ext.bind(this.onCancelForm, this)
			}, {
				text: this.btnSubmit,
				formBind: true, //only enabled once the form is valid
				//disabled: true,
				handler: Ext.bind(this.onSubmitForm, this)
			}],
			items: []
		};
		
		this.items = this.form = Ext.create('Ext.form.Panel', Ext.apply(formConfig, this.formConfig));
		this.callParent();
	},
	setMode: function(mode){
		this.mode = mode;
		return this;
	},
	_beforeShow: function(data){
		
	},
	show: function(data){
		this._beforeShow(data);
		this.setTitle(this.titles[this.mode]);
		this.callParent();
		if(this.mode == 'edit' && data){
			this.form.getForm().setValues(data);
		}else{
			this.form.getForm().reset();
		}
	},
	onSubmitForm: function(){
		var form = this.form.getForm();
		if (form.isValid()) {
			this.form.setLoading(true);
			form.submit({
				url: this.urls[this.mode],
				success: function(form, action) {
					this.onSuccess(form, action);
					this.form.setLoading(false);
				},
				failure: function(form, action) {
					this.onFailure(form, action);
					this.form.setLoading(false);
				},
				scope: this
			});
		}
	},
	onCancelForm: function(){
		this.close();
	},
	onSuccess: function(form, action){
		this.close();
		this.successCallback();
	},
	successCallback: function(){},
	onFailure: function(form, action){
		Ext.Msg.alert('Failed', action.result.msg);
	}
});