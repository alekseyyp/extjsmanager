/* 
 * @author Aleksey Prozhoga
 */
Ext.namespace('CustomLib.grid');
Ext.define('CustomLib.grid.ComboFilter', {
	extend: 'Ext.form.ComboBox',
	alias: 'widget.combofilter',
	name: 'filter',
	grid: null,
	url: null,
	fieldLabel: false,
	store: null,
	id: null,
	displayField: 'label',
    valueField: 'value',
    params: {},
	initComponent: function() {
        var me = this;
		if(this.id == null){
			this.id = Ext.id();
		}
		
		if(this.store == null && this.fields && this.url){
			Ext.define("Model"+this.id, {
				extend: 'Ext.data.Model',
	    		fields: this.fields
			});
			this.store = Ext.create('CustomLib.data.AjaxStore', {
				model: "Model"+this.id,
				url: this.url,
				root: 'list'
			})
			
		}
		me.callParent(arguments);
	},
	getGrid: function(){
		if(!this.grid){
			this.grid = this.findParentByType('grid');
		}
		return this.grid;
	},
	setQueryParam: function(key, value){
		this.params[key] = value;
		this.store.rebuildProxy(this.params);
	}
	
});