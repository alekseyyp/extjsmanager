/* 
 * @author Aleksey Prozhoga
 */
Ext.namespace('CustomLib.grid');

Ext.define('CustomLib.grid.SearchField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.searchfield',
	name: 'query',
	width:180,
	hasSearch : false,
	trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
    trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',

	gridId: null,
	initComponent: function() {
        var me = this;

        me.callParent(arguments);
        me.on('specialkey', function(f, e){
            if (e.getKey() == e.ENTER) {
                me.onTrigger2Click();
            }
        });
	},

    afterRender: function(){
        this.callParent();
        this.triggerCell.item(0).setDisplayed(false);
    },
	onTrigger1Click : function(){
		if(this.hasSearch){
            this.setRawValue('');
            this.getGrid().getStore().load({params : {query: null, start: 0}});
            this.getGrid().getView().refresh();
		    this.hasSearch = false;
		    var pagingtoolbar = this.getGrid().down("pagingtoolbar");
			
			if (pagingtoolbar) {
				
				pagingtoolbar.getStore().getProxy().extraParams['query'] = null;
			}
        	this.triggerCell.item(0).setDisplayed(false);
            this.updateLayout();
        }
    },
    onTrigger2Click : function(){
        var v = this.getRawValue();
        if(v.length < 1){
            this.onTrigger1Click();
            return;
        }
        this.getGrid().getStore().load({params : {query: v, start: 0}});
		this.getGrid().getView().refresh();
		
		var pagingtoolbar = this.getGrid().down("pagingtoolbar");
		
		if (pagingtoolbar) {
			
			pagingtoolbar.getStore().getProxy().extraParams['query'] = v;
		}
		
        this.hasSearch = true;
       this.triggerCell.item(0).setDisplayed(true);
        this.updateLayout();
    },
	getGrid: function(){
		if(!this.grid){
			this.grid = this.findParentByType('grid');
		}
		return this.grid;
	}
});