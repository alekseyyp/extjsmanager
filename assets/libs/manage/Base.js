/* 
 * @author Aleksey Prozhoga
 */
Ext.namespace('CustomLib.manage');

//Ext.require("CustomLib.window.WindowForm");

Ext.define('CustomLib.manage.Base',{
	dialogs:{},
	dialogConfigs:{},
	rendered: false,
	gird:null,
	constructor: function(config){
		if(config){
			var _this = this;
			_this = Ext.apply(_this, config);
		}
		Ext.QuickTips.init();
	},
	doRowAction: function(action, grid) {
		if (!action || !this.row_actions[action]) return false;
		
		var g = this.grid;
		
		if (grid) {
			
			var me = this;
			if (me[grid]) {
				g = me[grid];
			}
		}
		
		if (!g) return false;
		var sm = g.getSelectionModel();
		var d = sm.getLastSelected();
		if (d && d.data) {
			this.row_actions[action].call(this, d.data, sm);
		}
		return false;
	},
	doAction: function(action) {
		if (!action || !this.actions[action]) return false;
		this.actions[action].call(this);
		return false;
	},
	initDialog: function(type){
		if (!this.dialogs[type]){
			/*if(this.dialogConfigs[type]){
				var conf = this.dialogConfigs[type];
				Ext.apply(conf, {
					successCallback: Ext.bind(function() {
						this.grid.getSelectionModel().deselectAll();
						this.grid.getStore().load();
					}, this)
				});
				this.dialogs[type] = Ext.create('CustomLib.window.WindowForm', conf);
			}
			if (!this.dialogs[type]) return false;*/
			
			this.dialogs[type] = Ext.create(type, {
				successCallback: Ext.bind(function() {
					this.grid.getSelectionModel().deselectAll();
					this.grid.getStore().reload();
				}, this)
			});
			
			if (!this.dialogs[type]) return false;
			
		}
		return true;
	},
	doConfirmAction: function(cfg){
		var defaults = {
			title: 'Confirm',
			msg: '',
			fail: null,
			grid: this.grid,
			fn: function(btn){
				if ("yes" == btn){
					Ext.Ajax.request({
						url: this.url,
						params: this.params,
						success: Ext.bind(function(response){
							var obj = Ext.decode(response.responseText);
							if(obj.success == true){
								this.grid.getSelectionModel().deselectAll();
								this.grid.getStore().reload();
								if(obj.message){
									BaseM.msg(obj.message);
								}
							}else if(obj.alert_msg){
								Ext.Msg.alert("Alert", obj.msg);
							}else{
								Ext.Msg.alert("Confirmation", "Are you sure you wish to do this action?");
							}
						}, this)
					});
				}
			}
		};
		Ext.apply(defaults, cfg);
		Ext.MessageBox.confirm(defaults['title'], defaults['msg'], defaults['fn'], defaults)
	},
	_getButtonsAssoc: function(grid) {
            if (!grid) return {};
			if (grid.assocBtns) return grid.assocBtns;
            grid.assocBtns = {};

            var b = grid.buttons;
            var topToolbar = grid.getDockedItems("toolbar[dock=top]");
            if (topToolbar) {
                var all_btns = topToolbar[0].query("button")
                if (all_btns.length > 0) {
                    if (b && b.length > 0) {
                        b = b.concat(all_btns);
                    } else {
                        b = all_btns;
                    }
                }
            }

            if (b && b.length > 0) {
                var i,n;
                for(i=0; i<b.length; i++) {
                    n = b[i].btnName;
                    if (n) {
                        grid.assocBtns[n] = b[i];
                    }
                }
            }

            return grid.assocBtns;
    },
	setActiveButtons: function(list, grid) {
		if (!grid) {
			grid = this.grid;
		}
		var b = this._getButtonsAssoc(grid);
		for(var bn in list) {
			if (b[bn]) {
				b[bn].setDisabled(list[bn]);
			}
		}
	}
});