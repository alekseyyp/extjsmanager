Ext.namespace('CustomLib.form.field');

Ext.define('CustomLib.form.field.ComboBox', {
	extend: 'Ext.form.ComboBox',
	xtype: 'comboBox',
	queryMode: 'local',
	displayField: 'name',
	valueField: 'id',
	storeConfig: {},
	initComponent: function(){
		this.store = Ext.create('CustomLib.data.AjaxStore', Ext.apply({
			url: this.url,
			fields: [this.displayField, this.valueField]
		}, this.storeConfig));
		this.callParent();
	}
});