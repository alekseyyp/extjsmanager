Ext.ns("BaseM");

/**List of custom vtypes */
Ext.apply(Ext.form.field.VTypes, {
    //  vtype validation function
    addressurl: function(val, field) {
        return /[a-zA-Z0-9_-]/i.test(val);
    }//,
    // vtype Text property: The error text to display when the validation function returns false
    //addressurlText: LANG.addressurlText
});



Ext.apply(BaseM, {
	cache: {},
	msg: function(content, title){
		var msg = Ext.create("CustomLib.window.Message", {
			title: title,
			content: content
		});
	},
	require: function(namespace){
		//This functionality does not work
		return false;
		if(!BaseM.cache[namespace]){
			BaseM.cache[namespace] = true;
			if(typeof namespace != "object"){
				var url = "/assets/libs/" + namespace.replace(/\./g, "/").replace("CustomLib/", "") + ".js";
				
				var scripts = document.getElementsByTagName("script");
  				var scriptName = (document.currentScript || scripts[scripts.length - 1]).src;
				var list = Ext.getHead().query("script");
				var currentScript = null;
				for(var i=0; i < list.length; i++){
					if(list[i].src == scriptName){
						currentScript = list[i];
					}
				}
				if(currentScript != null){
					var fileref=document.createElement('script');
					fileref.setAttribute("type","text/javascript");
					fileref.setAttribute("src", url);
					document.head.insertBefore(fileref, currentScript);
				}else{
					alert("Can not load the script " + fileref);
				}
			}
		}
	}
});
