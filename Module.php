<?php
namespace ExtjsManager;
use Zend\Mvc\MvcEvent;
class Module
{
 	public function onBootstrap(MvcEvent $e)
	{
		if(!defined("EXTJSMANAGER_MODULE_FOLDER")){
			define("EXTJSMANAGER_MODULE_FOLDER", __DIR__);
		}
	}
 
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	public function getAutoloaderConfig()
	{
		return array(
				'Zend\Loader\ClassMapAutoloader' => array(
					__DIR__ . '/autoload_classmap.php',
				),
				'Zend\Loader\StandardAutoloader' => array(
						'namespaces' => array(
								__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
						),
				),
		);
	}
	
	public function getServiceConfig(){
		return array(
			'factories' => array(
				'ExtjsManager\FileCache' => function($sm){
					$adapter = \Zend\Cache\StorageFactory::factory(array(
						'adapter' => array(
							'name' => 'filesystem',
							'options' => array(
								'cache_dir' => getcwd()."/data/cache",
								'dir_level' => 1,
								'ttl'		=> 0,
								'dir_permission' => 0770,
								'file_permission' => 0660
							),
						),
						'plugins' => array(
							'exception_handler' => array('throw_exceptions' => true),
						)
					));
					return $adapter;
				}
			)
		);
	}
					
}