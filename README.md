ExtjsManager allows you to manage extjs files

INSTRUCTIONS
1) add a new repository to your composer.json

"repositories": [
   		{
			"type": "package",
	        "package": {
	            "name": "alekseyyp/extjsmanager",
	            "version": "0.3.4",
	            "source": {
	                "url": "https://bitbucket.org/alekseyyp/extjsmanager",
	                "type": "git",
	                "reference": "0.3.1"
	            },
	            "autoload": {
			        "psr-0": {
			            "ExtjsManager\\" : "src/"
			        },
			        "classmap": [
			            "./Module.php"
			        ]
			    }
	        }
		    
	     }
	]
	
require: {
	"alekseyyp/extjsmanager" : "0.3.*"
}

2) Add ExtjsManager module to your application.config.php

Example of using:

$this->ExtjsManager()
	->setTheme("neptune")
	->addNamespace("CustomLib.grid.SearchField")
	->addNamespace("CustomLib.window.Message")
	->addNamespace("CustomLib.manage.Base")
	->addNamespace("CustomLib.window.WindowForm")
	->addJs("/js/your-script.js")
	->apply();