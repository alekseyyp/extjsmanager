<?php
/**
* @author Aleksey Prozhoga
*/
namespace ExtjsManager\Controller\Plugin;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use ExtjsManager\Service\Assets\AssetsManager;

class ExtjsManager extends AbstractPlugin{
	
	const INITIAL_JAVASCRIPT_FILENAME = "app.js";
	
	private $service;
	private $view;
	private $jsScripts = array();
	private $initialJavascript = array();
	private $languageVars = array();
	private $namespaces = array();
	private $cssFiles = array();
	private $availableThemes = array("access", "classic", "classic-sandbox", "gray", "neptune");
	private $extjsFolder;
	
	/**
	 * @var boolean
	 */
	private $isDebug;
	
	public function __invoke()
	{
		return $this->init();
	}
	/**
	 * This function sets one of standard extjs theme
	 * @param string $name
	 * @throws \Exception
	 * @return ExtjsManager
	 */
	public function setTheme($name)
	{
		if(!in_array($name, $this->availableThemes)){
			throw new \Exception("The theme " . $name . " is not availbale. Available themes are " . implode(",", $this->availableThemes));
		}
		$this->addCss($this->extjsFolder . "resources/css/ext-all-" . $name . ".css");
		return $this;
	}
	/**
	 * This function adds the path of css file in the stack
	 * @param string $filePath
	 * @return ExtjsManager
	 */
	public function addCss($filePath)
	{
		$this->cssFiles[] = $filePath;
		return $this;
	}
	
	/**
	 * This function adds the path of js file in the stack
	 * @param string $filePath
	 * @return ExtjsManager
	 */
	public function addJs($filePath)
	{
		$this->jsScripts[] = $filePath;
		return $this;
	}
	
	public function addJsCode($jsCode)
	{
		$this->initialJavascript[] = $jsCode;
		return $this;
	}
	/**
	 * This function loads components from libs folder
	 * @param string $namespace
	 * @return \ExtjsManager\Controller\Plugin\ExtjsManager
	 */
	public function addNamespace($namespace)
	{
		$filePath = str_replace(array("CustomLib", "."), array("", "/"), $namespace).".js";
		$libsFolder = "/" . $this->service->getAssetsFolder() . "/" .AssetsManager::LIBS_FOLDER . "/";
		$this->addJs($libsFolder . $filePath);	
		return $this;
	}
	
	public function addLanguageVar($key, $value)
	{
		
		$this->languageVars[$key] = $value;
		return $this;
	}
	
	public function apply()
	{
		/* if($this->service->isFileExistsInPublicAssets(self::INITIAL_JAVASCRIPT_FILENAME) == false || $this->isDebug == true){
			//Add or Rewrite existing initial file
			$content = "";
			$len = sizeof($this->initialJavascript);
			if($len > 0){
				$content = implode("\n\n", $this->initialJavascript);
			}
			$this->service->addFileToPublicAssets(self::INITIAL_JAVASCRIPT_FILENAME, $content);
		} */
		$translator = $this->view->get("translate")->getTranslator();
		$lang = "var LANG = {\n";
		foreach($this->languageVars as $k=>$v)
		{
			$lang .= $k . ":'" . addslashes($translator->translate($v))."',\n";
		}
		$lang = rtrim($lang, ",\n") . "\n};";
		$this->initialJavascript[] = $lang;
		
		$content = "";
		$len = sizeof($this->initialJavascript);
		if($len > 0){
			$content = implode("\n\n", $this->initialJavascript);
			$inlineScript = $this->view->get("inlineScript");
			$inlineScript->appendScript($content);
		}
		
		$headScript = $this->view->get('headScript');
		$headLink = $this->view->get('headLink');
		$len = sizeof($this->jsScripts);
		for($i = 0; $i < $len; $i++){
			$headScript->appendFile($this->jsScripts[$i]);
		}
		$len = sizeof($this->cssFiles);
		for($i = 0; $i < $len; $i++){
			$headLink->appendStylesheet($this->cssFiles[$i]);
		}
	}
	
	private function init()
	{
		$sl = $this->getController()->getServiceLocator();
		$this->service = $sl->get("ExtjsManager");
		$this->view = $sl->get('viewhelpermanager');
		
		$this->extjsFolder =  "/" . $this->service->getAssetsFolder() . "/" . AssetsManager::EXTJS_FOLDER . "/";
		$this->isDebug = ($this->service->getMode() == AssetsManager::MODE_DEBUG ? true : false);
		$this->addJs($this->extjsFolder . "ext-all" . ($this->isDebug ? "-debug" : "") . ".js");
		//$this->addJs("/" . $this->service->getAssetsFolder() . "/" . self::INITIAL_JAVASCRIPT_FILENAME);
		$libsFolder = "/" . $this->service->getAssetsFolder() . "/" .AssetsManager::LIBS_FOLDER;
		$this->addJs( $libsFolder . "/base.js");
		
		//$this->initialJavascript[] = "Ext.namespace('App');";
		return $this;
	}
}