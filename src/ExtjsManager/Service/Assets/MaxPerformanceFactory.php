<?php
/**
* @author Aleksey Prozhoga
* This mode is not implemented
*/
namespace ExtjsManager\Service\Assets;

class MaxPerformanceFactory extends AbstractFactory
{
	protected function getMode(){
		return AssetsManager::MODE_MAX_PERFORMANCE;
	}
}