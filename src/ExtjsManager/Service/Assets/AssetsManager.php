<?php
/**
* @author Aleksey Prozhoga
*/
namespace ExtjsManager\Service\Assets;

use ExtjsManager\Controller\Plugin\ExtjsManager;
class AssetsManager
{
	const ASSETS_FOLDER_PERMISSIONS = 0775;
	const MODE_DEBUG 			= "debug";
	const MODE_LIVE 			= "live";
	const MODE_MAX_PERFORMANCE	= "maxPerformance";
	const DIR_SEP				= DIRECTORY_SEPARATOR;

	const EXTJS_FOLDER			= "extjs";
	const LIBS_FOLDER			= "libs";

	//Internal assets constants

	const INTERNAL_ASSETS_FOLDER 	= "assets";
	const INTERNAL_EXTJS_FOLDER 	= "extjs";

	const INTERNAL_LIBS_FOLDER		= "libs";

	const LIBS_VERSION_FILENAME		= "version.txt";

	const CACHE_VERSION_KEY			= "extjslibs-version";


	private $publicFolder 	= "public";
	private $assetsFolder 	= "assets";
	private $mode;
	private $basePath;

	private $cache;

	public function __construct($mode, $cacheService)
	{
		$this->cache = $cacheService;
		$this->changeMode($mode);
	}

	/**
	 * This function returns the name of the assets folder in the public
	 * @return string
	 */
	public function getAssetsFolder()
	{
		return $this->assetsFolder;
	}

	/**
	 * This function returns the current mode
	 * @return string
	 */
	public function getMode(){
		return $this->mode;
	}

	/**
	 * This function checks if any file exists in the public assets folder
	 * @param string $filePath this is a relational path
	 * @return boolean
	 */
	public function isFileExistsInPublicAssets($filePath){
		$assetsFolder = $this->getBasePath() . self::DIR_SEP . $this->publicFolder . self::DIR_SEP . $this->assetsFolder . self::DIR_SEP;

		return file_exists($assetsFolder . $filePath);
	}

	/**
	 * This function adds a new file with a specified content to the public assets folder
	 * @param string $filePath
	 * @param string $content
	 */
	public function addFileToPublicAssets($filePath, $content){
		$assetsFolder = $this->getBasePath() . self::DIR_SEP . $this->publicFolder . self::DIR_SEP . $this->assetsFolder . self::DIR_SEP;

		$fr = fopen($assetsFolder . $filePath, "w+");
		fwrite($fr, $content);
		fclose($fr);
	}

	private function changeMode($mode)
	{
		$this->mode = $mode;
		if($mode == self::MODE_DEBUG ||
			$mode == self::MODE_LIVE
		)
		{
			$publicFolder = $this->getBasePath() . self::DIR_SEP . $this->publicFolder;
			//Check if public and assets folder exist
			if(file_exists($publicFolder)){
				$assetsFolder = $publicFolder . self::DIR_SEP . $this->assetsFolder;
				if(file_exists($assetsFolder) == false){
					if(mkdir($assetsFolder, self::ASSETS_FOLDER_PERMISSIONS) == false){
						throw new \Exception("ExtjsManager can not create assets folder: " . $assetsFolder);
					}
				}
				exec("chmod -R " . self::ASSETS_FOLDER_PERMISSIONS . " " . $assetsFolder);
			}else{
				throw new \Exception("You have different public folder from " . $publicFolder . ". Please specify your folder name in ExtjsManager");
			}

			//Check if extjs folder exists
			$extjsFolder = $assetsFolder . self::DIR_SEP . self::EXTJS_FOLDER;
			if(file_exists($extjsFolder) == false){
				$this->copy(EXTJSMANAGER_MODULE_FOLDER . self::DIR_SEP .
							self::INTERNAL_ASSETS_FOLDER . self::DIR_SEP . self::INTERNAL_EXTJS_FOLDER,
							$extjsFolder);

				if(file_exists($extjsFolder) == false){
					throw new \Exception("ExtjsManager can not copy ExtJs to " . $extjsFolder);
				}
			}

			$libsFolder = $assetsFolder . self::DIR_SEP . self::LIBS_FOLDER . self::DIR_SEP;

			if(file_exists($libsFolder) == false){
				if(mkdir($libsFolder, self::ASSETS_FOLDER_PERMISSIONS) == false){
					throw new \Exception("ExtjsManager can not create libs folder: " . $libsFolder);
				}

				$this->copy(EXTJSMANAGER_MODULE_FOLDER . self::DIR_SEP .
						self::INTERNAL_ASSETS_FOLDER . self::DIR_SEP . self::INTERNAL_LIBS_FOLDER,
						$libsFolder);

				if(file_exists($libsFolder) == false){
					throw new \Exception("ExtjsManager can not copy Libs to " . $libsFolder);
				}
			}
			//Check libs version and update libs if necesurry
			$fr = fopen(EXTJSMANAGER_MODULE_FOLDER . self::DIR_SEP .
						self::INTERNAL_ASSETS_FOLDER . self::DIR_SEP . self::INTERNAL_LIBS_FOLDER .
						self::DIR_SEP . self::LIBS_VERSION_FILENAME, "r");
			$currentVersion = trim(fread($fr, 10));
			fclose($fr);
			if($this->cache->hasItem(self::CACHE_VERSION_KEY) == false ||
					$this->cache->getItem(self::CACHE_VERSION_KEY) != $currentVersion ||
					$this->mode == self::MODE_DEBUG){
				$this->copy(EXTJSMANAGER_MODULE_FOLDER . self::DIR_SEP .
						self::INTERNAL_ASSETS_FOLDER . self::DIR_SEP . self::INTERNAL_LIBS_FOLDER,
						$libsFolder);

				$this->cache->setItem(self::CACHE_VERSION_KEY, $currentVersion);
			}
		}
	}

	private function getBasePath(){
		if(!$this->basePath){
			$this->basePath = getcwd();
		}
		return $this->basePath;
	}

	private function copy($src,$dst) {
		$dir = opendir($src);
		@mkdir($dst, self::ASSETS_FOLDER_PERMISSIONS);
		while(false !== ( $file = readdir($dir)) ) {
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($src . '/' . $file) ) {
					$this->copy($src . '/' . $file, $dst . '/' . $file);
				}
				else {
					copy($src . '/' . $file, $dst . '/' . $file);
				}
				chmod($dst . '/' . $file, self::ASSETS_FOLDER_PERMISSIONS);
			}
		}
		closedir($dir);
	}
}