<?php
/**
* @author Aleksey Prozhoga
*/
namespace ExtjsManager\Service\Assets;

class LIveFactory extends AbstractFactory
{
	protected function getMode(){
		return AssetsManager::MODE_LIVE;
	}
}