<?php
/**
* @author Aleksey Prozhoga
*/
namespace ExtjsManager\Service\Assets;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class AbstractFactory implements FactoryInterface
{
	abstract protected function getMode();
	
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$assetsManager = new AssetsManager($this->getMode(), $serviceLocator->get("ExtjsManager\FileCache"));
		return $assetsManager;
	}
	
	
}