<?php
/**
* @author Aleksey Prozhoga
*/
namespace ExtjsManager\Service\Assets;

class DebugFactory extends AbstractFactory
{
	protected function getMode(){
		return AssetsManager::MODE_DEBUG;
	}
}